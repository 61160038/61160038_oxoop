import java.util.InputMismatchException;
import java.util.Scanner;

public class Game {
	public Scanner sc = new Scanner(System.in);
	public int row;
	public int col;
	Table table = null;
	Player o = null;
	Player x = null;

	public Game() {
		this.o = new Player('o');
		this.x = new Player('x');
	}

	public void run() {
		for (;;) {
			this.runOnce();
			if (!askContinue()) {
				return;
			}
		}
	}

	private boolean askContinue() {
		for (;;) {
			System.out.print("Continue y/n ? : ");
			String ans = sc.next();
			if (ans.equals("n")) {
				System.out.println("The End!!!");
				return false;
			} else if (ans.equals("y")) {
				return true;
			}
		}
	}

	public int getRandomNumber(int min, int max) {
		return (int) ((Math.random() * (max - min)) + min);
	}

	public void newGame() {
		if (getRandomNumber(1, 100) % 2 == 0) {
			this.table = new Table(o, x);
		} else {
			this.table = new Table(x, o);
		}
	}

	public void runOnce() {
		this.showWelcome();
		this.newGame();
		for (;;) {
			this.showTable();
			this.showTurn();
			this.inputRowCol();

			if (table.checkWin()) {
				this.showResult();
				this.showState();
				return;
			}

			table.switchPlayer();
		}

	}

	private void showResult() {
		if (table.getWinner() != null) {
			this.showTable();
			showWin();
		} else {
			this.showTable();
			showeDraw();
		}
	}

	private void showeDraw() {
		System.out.println(" Draw!!!! ");
	}

	private void showWin() {
		System.out.println(table.getWinner().getName() + " Win!!!!");
	}

	private void showState() {
		System.out.println(o.getName() + "(win,lost,draw) :" + o.getWin() + o.getLost() + o.getDraw());
		System.out.println(x.getName() + "(win,lost,draw) :" + x.getWin() + x.getLost() + x.getDraw());

	}

	public void showWelcome() {
		System.out.println("Welcome to XO gmae");
	}

	public void showTable() {
		char[][] data = this.table.getData();
		for (int row = 0; row < data.length; row++) {
			System.out.print("|");
			for (int col = 0; col < data[row].length; col++) {
				System.out.print(" " + data[row][col] + " |");
			}
			System.out.println("");
		}
	}

	public void showTurn() {
		System.out.println(table.getCurrentPlayer().name + " Turn");
	}

	public void input() {
		for (;;) {
			try {
				System.out.print("Please input row col : ");
				this.row = sc.nextInt();
				this.col = sc.nextInt();
				if(sc.hasNext()) {
					System.out.println("Please input Row Col 1 2 3 !!!");
					continue;
				}
				return;
			} catch (InputMismatchException iE) {
				sc.next();
				System.out.println("Please input number 1 2 3 !!!");
			}
		}
	}

	public void inputRowCol() {
		for (;;) {
			this.input();
			try {
				if (table.setRowCol(row, col)) {
					return;
				}
			} catch (java.lang.ArrayIndexOutOfBoundsException e) {
				System.out.println("Please input number 1 2 3 !!!");
			}

		}

	}

}
