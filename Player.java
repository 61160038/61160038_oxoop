
public class Player {
	public char name;
	public int win;
	public int lost;
	public int draw;

	public int getWin() {
		return win;
	}

	public int getLost() {
		return lost;
	}
	public void win() {
		this.win++;
	}
	public void lost() {
		this.lost++;
	}public void draw() {
		this.draw++;
	}
	

	public int getDraw() {
		return draw;
	}

	public Player(char name) {
		this.name = name;
	}

	public char getName() {
		return name;
	}

	public void setName(char name) {
		this.name = name;
	}

	public String toString() {
		return "Player [name=" + name + "]";
	}
}
